import counterSlice from '@/reducers/counter';
import postSlice from '@/reducers/post';
import userSlice from '@/reducers/users';
import { configureStore, combineReducers } from '@reduxjs/toolkit';

const reducer = combineReducers({
  counter: counterSlice,
  posts: postSlice,
  users: userSlice,
});

const store = configureStore({
  reducer,
});

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
