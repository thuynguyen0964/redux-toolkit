import { AddPost, PostList } from './components/posts';

function App() {
  return (
    <section className='App'>
      <AddPost />
      <PostList />
    </section>
  );
}

export default App;
