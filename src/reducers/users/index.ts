import api from '@/api';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

type UserProps = {
  id: number;
  name: string;
};

export const getUsers = createAsyncThunk('user/getUsers', async () => {
  const res = await api.get('users');
  return res.data;
});

const initialState: UserProps[] = [];

const userSlice = createSlice({
  initialState,
  name: 'users',
  reducers: {},
  extraReducers(builder) {
    builder.addCase(getUsers.fulfilled, (_, action) => {
      return action.payload;
    });
  },
});

export const actions = userSlice.actions;
export default userSlice.reducer;
