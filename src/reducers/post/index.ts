import api from '@/api';
import { cloneDeep } from '@/utils/contants';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sub } from 'date-fns';

type Props = {
  posts: IPosts[] | IResponse[];
  status: IStatus;
  error: string | null;
};

export const getPosts = createAsyncThunk('posts/getPosts', async () => {
  const res = await api.get<IResponse[]>('/posts?_page=1&_limit=5');
  return res.data;
});

export const addNewPost = createAsyncThunk(
  'posts/addNewPost',
  async (body: IPosts) => {
    const res = await api.post('posts', body);
    return res.data;
  }
);

const initialState: Props = {
  posts: [],
  status: 'idle',
  error: null,
};

const postSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    createPost: (state, action) => {
      const clonePost = cloneDeep<IPosts[]>(state.posts);
      clonePost.push(action.payload);
      state.posts = clonePost;
    },
    addReactions: (state, action) => {
      const { postId, reaction }: PayloadReact = action.payload;
      const postExit = state.posts.find((post) => post.id === postId);
      if (postExit) {
        postExit.react[reaction as ReactionType]++;
      }
    },
  },

  extraReducers(builder) {
    builder
      .addCase(getPosts.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getPosts.fulfilled, (state, action) => {
        state.status = 'idle';
        let min = 1;
        const loaderPost = action.payload.map((post) => {
          post.date = sub(new Date(), { minutes: min++ }).toISOString();
          post.react = {
            heart: 0,
            like: 0,
            rocket: 0,
          };
          return post;
        });

        state.posts = state.posts.concat(loaderPost);
      })
      .addCase(getPosts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.payload as string;
      })
      .addCase(addNewPost.fulfilled, (state, action) => {
        action.payload.id = Number(action.payload.id);
        action.payload.userId = Number(action.payload.userId);
        action.payload.date = new Date().toISOString();
        action.payload.react = {
          heart: 0,
          rocket: 0,
          like: 0,
        };
        state.posts.push(action.payload);
      });
  },
});

export const { createPost, addReactions } = postSlice.actions;
export default postSlice.reducer;
