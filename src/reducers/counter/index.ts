import { createSlice } from '@reduxjs/toolkit';

type Props = {
  count: number;
};
const initialState: Props = {
  count: 0,
};

const counterSlice = createSlice({
  initialState,
  name: 'counter',
  reducers: {
    increase: (state) => {
      state.count += 1;
    },
    decrease: (state) => {
      state.count -= 1;
    },
    increaseByAmount: (state, action) => {
      state.count += action.payload;
    },
  },
});

export const { decrease, increase, increaseByAmount } = counterSlice.actions;
export default counterSlice.reducer;
