interface IPosts {
  id: string | number;
  title: string;
  body: string;
  userId: number;
  date?: string;
  react: {
    heart: number;
    like: number;
    rocket: number;
  };
}

type ReactionType = 'heart' | 'like' | 'rocket';

interface PayloadReact {
  postId: string;
  reaction: typeof ReactionType;
}

type IStatus = 'loading' | 'idle' | 'failed';

interface IResponse extends IPosts {
  id: number;
  title: string;
  body: string;
  userId: number;
}
