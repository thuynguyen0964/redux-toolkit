const uuid = (): string => crypto.randomUUID();

const cloneDeep = <T = void>(params: T): T => {
  const newObject = JSON.parse(JSON.stringify(params));
  return newObject;
};
export { uuid, cloneDeep };
