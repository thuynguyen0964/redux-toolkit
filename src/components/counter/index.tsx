import { decrease, increase, increaseByAmount } from '@/reducers/counter';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/store';

export default function Counter() {
  const { count } = useSelector((state: RootState) => state.counter);
  const dispatch = useDispatch();
  return (
    <>
      <p>{count}</p>
      <div>
        <button onClick={() => dispatch(increase())}>+</button>
        <button onClick={() => dispatch(decrease())}>-</button>
        <button onClick={() => dispatch(increaseByAmount(5))}>Custom</button>
      </div>
    </>
  );
}
