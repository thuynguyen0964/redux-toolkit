import { RootState } from '@/store';
import { useSelector } from 'react-redux';

type Props = {
  id: number;
};

const PostAuthor = ({ id }: Props) => {
  const authors = useSelector((state: RootState) => state.users);
  const user = authors.find((author) => author.id === +id);

  return <span>by {(user && user.name) || 'Unknow author'}</span>;
};

export default PostAuthor;
