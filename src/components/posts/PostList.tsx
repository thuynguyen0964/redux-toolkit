/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from '@/store';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { getPosts } from '@/reducers/post';
import PostsExcerpt from './PostExpert';
import { nanoid } from '@reduxjs/toolkit';
import { getUsers } from '@/reducers/users';

const PostList = () => {
  const { posts, status, error } = useSelector(
    (state: RootState) => state.posts
  );
  const dispatch = useDispatch<any>();
  useEffect(() => {
    if (status === 'idle') {
      dispatch(getPosts());
      dispatch(getUsers());
    }
  }, [dispatch]);

  let content;
  if (status === 'loading') {
    content = <p>"Loading..."</p>;
  } else if (status === 'idle') {
    content = posts.map((post) => <PostsExcerpt key={nanoid(5)} post={post} />);
  } else if (status === 'failed') {
    content = <p>{error}</p>;
  }

  return (
    <section>
      <h2>List of Post</h2>
      {content}
    </section>
  );
};

export default PostList;
