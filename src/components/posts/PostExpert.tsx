import PostAuthor from './PostAuthor';
import TimeAgo from './PostTime';
import ReactionButtons from './Reaction';

type Props = {
  post: IPosts | IResponse;
};

const PostsExcerpt = ({ post }: Props) => {
  return (
    <article>
      <h3>{post.title}</h3>
      <p>{post.body.substring(0, 100)}</p>
      <p className='postCredit'>
        <PostAuthor id={post.userId} />
        <TimeAgo timestamp={post.date as string} />
      </p>
      <ReactionButtons post={post} />
    </article>
  );
};
export default PostsExcerpt;
