import { useDispatch } from 'react-redux';
import { addReactions } from '@/reducers/post';

const reactEmoji = {
  heart: '🤎',
  like: '👍🏼',
  rocket: '🚀',
};

function Reaction({ post }: { post: IPosts }) {
  const dispatch = useDispatch();
  const reactBtn = Object.entries(reactEmoji).map(([name, emoji]) => (
    <button
      key={name}
      type='button'
      className='reactionButton'
      onClick={() =>
        dispatch(addReactions({ postId: post.id, reaction: name }))
      }
    >
      {emoji} {post.react[name as ReactionType]}
    </button>
  ));

  return <div>{reactBtn}</div>;
}

export default Reaction;
