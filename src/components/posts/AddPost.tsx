import { addNewPost } from '@/reducers/post';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { nanoid } from '@reduxjs/toolkit';
import { RootState } from '@/store';
import { sub } from 'date-fns';

const timeWrite = sub(new Date(), { minutes: 1 }).toISOString();
const react = {
  heart: 0,
  like: 0,
  rocket: 0,
};

type Props = {
  id?: string;
  title: string;
  body: string;
  userId: number;
};

type ChangeDOM = HTMLSelectElement | HTMLInputElement | HTMLTextAreaElement;

const initVal: Props = { title: '', body: '', userId: 0 };

const AddPost = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const dispatch = useDispatch<any>();
  const author = useSelector((state: RootState) => state.users);

  const [formVal, setFormVal] = useState<Props>(initVal);

  const handleChange = (e: React.ChangeEvent<ChangeDOM>) => {
    setFormVal((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const handleCreatePost = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const payload = { id: nanoid(5), ...formVal, date: timeWrite, react };
      dispatch(addNewPost(payload)).unwrap();
      setFormVal(initVal);
    } catch (error) {
      console.log(error);
    }
  };

  const isDisabled = formVal.title === '' || formVal.body === '';
  return (
    <section>
      <h2>Add a New Post</h2>
      <form onSubmit={handleCreatePost} autoComplete='off'>
        <label htmlFor='postTitle'>Post Title:</label>
        <input
          type='text'
          id='title'
          name='title'
          value={formVal.title}
          onChange={handleChange}
        />
        <label htmlFor='body'>Content:</label>
        <textarea
          id='body'
          name='body'
          value={formVal.body}
          onChange={handleChange}
        />
        <label htmlFor='authorId'>Author:</label>
        <select name='userId' onChange={handleChange} id='authorId'>
          <option defaultValue={initVal.userId}>--Select author--</option>
          {author.map((man) => (
            <option key={man.id} value={man.id}>
              {man.name}
            </option>
          ))}
        </select>
        <button type='submit' disabled={isDisabled}>
          Save Post
        </button>
      </form>
    </section>
  );
};

export default AddPost;
